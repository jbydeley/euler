package multiple

import "testing"

func TestSmallestMultipleOfSmall(t *testing.T) {
	expected := 2520
	actual := SmallestMultipleOf(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestSmallestMultipleOfLarge(t *testing.T) {
	expected := 232792560
	actual := SmallestMultipleOf(20)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
