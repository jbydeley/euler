package multiple

func SmallestMultipleOf(n int) int {
	lcm := 1
	for i := 1; i < n; i++ {
		sum := lcm
		for sum%i != 0 {
			sum += lcm
		}
		lcm = sum
	}
	return lcm
}
