package prime

func GetLargestPrimeFactor(n int64) int64 {
	var max, i int64

	for i = 1; i <= n; i++ {
		if n%i == 0 {
			n /= i
			max = i
		}
	}
	return max
}
