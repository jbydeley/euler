package prime

import "testing"

func TestGetLargestPrimeFactorSmall(t *testing.T) {
	var expected int64 = 29
	actual := GetLargestPrimeFactor(13195)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestGetLargestPrimeFactorLarge(t *testing.T) {
	var expected int64 = 6857
	actual := GetLargestPrimeFactor(600851475143)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
