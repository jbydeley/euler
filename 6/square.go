package square

func SquareOfSums(input int) int {
	t := (input * (input + 1)) / 2
	return t * t
}

func SumOfSquares(input int) int {
	return input * (input + 1) * (2*input + 1) / 6
}

func DifferenceOfSums(input int) int {
	return SquareOfSums(input) - SumOfSquares(input)
}
