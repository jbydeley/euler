package square

import "testing"

func TestSumOfSquaresSmall(t *testing.T) {
	expected := 385
	actual := SumOfSquares(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestSquaresOfSumsSmall(t *testing.T) {
	expected := 3025
	actual := SquareOfSums(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestDifferenceOfSumsSmall(t *testing.T) {
	expected := 2640
	actual := DifferenceOfSums(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestSumOfSquaresLarge(t *testing.T) {
	expected := 338350
	actual := SumOfSquares(100)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestSquaresOfSumsLarge(t *testing.T) {
	expected := 25502500
	actual := SquareOfSums(100)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestDifferenceOfSumsLarge(t *testing.T) {
	expected := 25164150
	actual := DifferenceOfSums(100)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
