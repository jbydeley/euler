package fib

import "testing"

func SumOfEvenFibs10(t *testing.T) {
	expected := 10
	actual := SumOfEvenFibs(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func SumOfEvenFibs4000000(t *testing.T) {
	expected := 4613732
	actual := SumOfEvenFibs(4000000)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
