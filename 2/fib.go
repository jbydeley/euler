package fib

func SumOfEvenFibs(n int) (sum int) {
	j := 2
	for i := 1; i < n; {
		if j%2 == 0 {
			sum += j
		}
		i, j = j, i+j
	}
	return sum
}
