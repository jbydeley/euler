package palindrome

import "testing"

func TestFindLargestPalindromeSmall(t *testing.T) {
	expected := 9009
	actual := FindLargestPalindrome(99)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestFindLargestPalindromeLarge(t *testing.T) {
	expected := 906609
	actual := FindLargestPalindrome(999)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
