package palindrome

import (
	"strconv"
	"strings"
)

func FindLargestPalindrome(n int) int {
	for x := n; x > 900; x-- {
		for y := n; y > 900; y-- {
			if strconv.Itoa(x*y) == inReverse(x*y) {
				return x * y
			}
		}
	}
	return 0
}

func inReverse(val int) string {
	s := strconv.Itoa(val)
	sa := strings.Split(s, "")

	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		sa[i], sa[j] = sa[j], sa[i]
	}
	return strings.Join(sa, "")
}
