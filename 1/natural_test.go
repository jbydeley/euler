package natural

import "testing"

func TestNaturalsBelow10(t *testing.T) {
	expected := 23
	actual := GetSumOfNaturals(10)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestNaturalsBelow100(t *testing.T) {
	expected := 233168
	actual := GetSumOfNaturals(1000)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
