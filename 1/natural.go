package natural

func GetSumOfNaturals(n int) (sum int) {
	for idx := 1; idx < n; idx++ {
		if idx%3 == 0 {
			sum += idx
		} else if idx%5 == 0 {
			sum += idx
		}
	}
	return sum
}
