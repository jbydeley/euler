package prime

import "testing"

func TestIsPrimeWithPrime(t *testing.T) {
	expected := true
	actual := isPrime(13)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestIsPrimeWithNotPrime(t *testing.T) {
	expected := false
	actual := isPrime(6)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestLowPrime(t *testing.T) {
	expected := 13
	actual := GetXPrime(6)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}

func TestHighPrime(t *testing.T) {
	expected := 104743
	actual := GetXPrime(10001)

	if actual != expected {
		t.Errorf("%v was not %v", actual, expected)
	}
}
