package prime

import "math"

func GetXPrime(n int) (prime int) {
	count := 0
	for i := 2; count < n; i += 1 {
		if isPrime(i) {
			prime = i
			count++
		}
	}
	return
}

func isPrime(n int) bool {
	if n < 2 {
		return false
	}

	to := int(math.Sqrt(float64(n))) + 1

	for i := 2; i < to; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}
